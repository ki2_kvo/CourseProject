﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class NewMath
    {
        public static Vector2f Normalize(Vector2f vec)
        {
            return vec / (float)Math.Sqrt(vec.X * vec.X + vec.Y * vec.Y);
        }

        public static double RadiansToDegrees(double value)
        {
            return value * 180 / Math.PI;
        }

        public static float Multiply(Vector2f vec1, Vector2f vec2)
        {
            return vec1.X * vec2.X + vec1.Y * vec2.Y;
        }
        // Метод принимает координаты 2 точек отрезка и информацию о прямоугольнике для поиска пересечения
        public static bool Intersects(Vector2f point1, Vector2f point2, FloatRect rect)
        {
            Vector2f point3 = new Vector2f(rect.Left, rect.Top);
            Vector2f point4 = new Vector2f(rect.Left + rect.Width, rect.Top + rect.Height);
            Vector2f point5 = new Vector2f(rect.Left, rect.Top + rect.Height);
            Vector2f point6 = new Vector2f(rect.Left + rect.Width, rect.Top);

            bool intersect1 = Intersects(point1, point2, point3, point4);
            bool intersect2 = Intersects(point1, point2, point5, point6);

            return intersect1 || intersect2;
        }

        public static bool Intersects(Vector2f Point1, Vector2f Point2, Vector2f Point3, Vector2f Point4)
        {
            Vector2f line1 = Point1 - Point2;
            Vector2f line2 = Point3 - Point4;
            Vector2f line3 = Point1 - Point4;
            Vector2f line4 = Point1 - Point3;
            Vector2f line5 = Point3 - Point1;
            Vector2f line6 = Point3 - Point2;

            if (VectorProduct(line1, line4) * VectorProduct(line1, line3) < 0 && VectorProduct(line2, line5) * VectorProduct(line2, line6) < 0)
            {
                    return true;
            }
            else
            {
                return false;
            }
        }

        public static float VectorProduct(Vector2f line1, Vector2f line2)
        {
            return line1.X * line2.Y - line1.Y * line2.X;
        }

        public static Vector2f Abs(Vector2f vec)
        {
            return new Vector2f(Math.Abs(vec.X), Math.Abs(vec.Y));
        }
    }
}
