﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class GameTimer
    {
        public static Clock clock;
        public static uint time;

        public static void Start()
        {
            clock = new Clock();
            time = (uint)clock.ElapsedTime.AsMilliseconds();
        }
        public static void Restart()
        {
            clock.Restart();
            time = (uint)clock.ElapsedTime.AsMilliseconds();
        }
    }
}
