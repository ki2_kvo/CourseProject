﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class UserInterface
    {
        protected RectangleShape UIPanel;

        protected Font font = new Font("..\\Content\\Fonts\\COMIC.TTF");
        protected Text textHealth;

        public UserInterface()
        {
            textHealth = new Text("Здоровье: " + Game.Player.HealthPoints, font);
        }
        public void Update()
        {
            textHealth.Position = new Vector2f(GameWindow.viewRect.Left, GameWindow.viewRect.Top);
            textHealth.DisplayedString = "Здоровье: " + Game.Player.HealthPoints;
        }
        public void Draw()
        {
            GameWindow.window.Draw(textHealth);
        }
    }
}
