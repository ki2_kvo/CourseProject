﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SFML.Graphics;
using SFML.Window;

namespace ClassLibrary
{
    public class Game
    {
        protected static World world;
        protected static Character player;
        protected static UserInterface UIPanel;
        protected static List<Creature> enemy;
        protected static List<Bullet> bullet;

        public static void Load()
        {
            MessageSystem.DisplayHelp();
            GameTimer.Restart();

            world = new World();
            enemy = new List<Creature>();
            bullet = new List<Bullet>();
            enemy.Add(new Enemy(new Vector2f(3 * 32, 4 * 32)));
            enemy.Add(new Enemy(new Vector2f(650, 70)));
            enemy.Add(new Enemy(new Vector2f(170, 70)));
            player = new Character(new Vector2f(350, 70));
            UIPanel = new UserInterface();
        }
        public static void Clear()
        {
            player.Remove();
            enemy.Clear();
            bullet.Clear();
        }

        public static World World
        {
            get { return world; }
        }
        public static Character Player
        {
            get { return player; }
        }
        public static List<Creature> Enemy
        {
            get { return enemy; }
        }
        public static List<Bullet> Bullet
        {
            get { return bullet; }
        }

        public static void Update(float interpolation)
        {
            player.Update();
            world.Update(player, enemy);
            for (int i = 0; i < enemy.Count; i++)
            {
                enemy[i].Update();

                if (enemy[i].HealthPoints <= 0)
                {
                    enemy.RemoveAt(i);
                }
            }
            
            for (int i = 0; i < bullet.Count; i++)
            {
                bullet[i].Update(interpolation);

                if (bullet[i].IsDestroyed)
                {
                    bullet.RemoveAt(i);
                }
            }
            UIPanel.Update();
        }
        public static void Draw()
        {
            world.Draw();

            foreach (var item in enemy)
            {
                item.Draw();
            }
            foreach (var item in bullet)
            {
                item.Draw();
            }

            player.Draw();
            UIPanel.Draw();
        }
    }
}
