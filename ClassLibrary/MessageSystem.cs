﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassLibrary
{
    public class MessageSystem
    {
        public static void DisplayHelp()
        {
            MessageBox.Show("Управление:\nEscape - Вернуться в меню\nAD - Движение персонажа\nSpace - Прыжок\nЛевая клавиша мыши - Выстрел", "Обучение", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void DisplayWinEnd()
        {
            MessageBox.Show("Победа.\n\nВрагов больше нету.", "Уровень пройден!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void DisplayLoseEnd()
        {
            MessageBox.Show("Потрачено.", "Уровень не пройден!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
