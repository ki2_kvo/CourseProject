﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Character : Creature
    {
        public bool IsAlive { set; get; }

        public Character() : base()
        {
        }
        public Character(Vector2f start) : base(start)
        {
            IsAlive = true;
            healthPoints = 100;
            MoveSpeed = 4f;
            MoveSpeedAcceleration = 1f;
            JumpHeight = -10f;
            rectangle.Texture = new Texture(Content.texture["Hero"]);
            rectangle.TextureRect = new IntRect(0, 0, 32, 64);

            GameWindow.window.MouseButtonPressed += Window_MouseButtonPressed;
        }
        public void Remove()
        {
            GameWindow.window.MouseButtonPressed -= Window_MouseButtonPressed;
        }

        private void Window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            if (e.Button == Mouse.Button.Left)
            {
                CreateBullet();
            }
        }
        public void CreateBullet()
        {
            Vector2f startShotPoint;
            Vector2f windowOffset = new Vector2f(GameWindow.viewRect.Left, GameWindow.viewRect.Top);
            Vector2f endShotPoint = (Vector2f)Mouse.GetPosition(GameWindow.window) + windowOffset;
            if (dir == DirectionSide.Right)
            {
                startShotPoint = new Vector2f(Position.X + Rectangle.Size.X, Position.Y + Rectangle.Size.Y / 3);
            }
            else
            {
                startShotPoint = new Vector2f(Position.X, Position.Y + Rectangle.Size.Y / 3);
            }
            Vector2f normalShotVector = NewMath.Normalize(endShotPoint - startShotPoint);

            Game.Bullet.Add(new Bullet(normalShotVector));
        }

        public override void Update()
        {
            UpdateMovement();

            MakeMove();

            UpdateTexture();

            rectangle.Position = new Vector2f(rectangleInfo.Left, rectangleInfo.Top);

            CheckHealth();
        }
        
        public override void Collision(DirectionType type)
        {
            foreach (var item in world.Map.Chunks)
            {
                if (item.playerHere)
                {
                    Tile tile = item.GetTileByPosition(rectangleInfo);

                    if (tile != null)
                    {
                        if (movementSpeed.X > 0 && type == DirectionType.AlongX)
                        {
                            rectangleInfo.Left = tile.Position.X - rectangleInfo.Width;
                        }
                        else if (movementSpeed.X < 0 && type == DirectionType.AlongX)
                        {
                            rectangleInfo.Left = tile.Position.X + tile.Size.X;
                        }
                        if (movementSpeed.Y > 0 && type == DirectionType.AlongY)
                        {
                            rectangleInfo.Top = tile.Position.Y - rectangleInfo.Height;
                            movementSpeed.Y = 0;
                            onGround = true;
                        }
                        else if (movementSpeed.Y < 0 && type == DirectionType.AlongY)
                        {
                            rectangleInfo.Top = tile.Position.Y + tile.Size.Y;
                            movementSpeed.Y = 0;
                        }
                    }
                }
            }
        }
        public override void UpdateTexture()
        {
            foreach (var enemy in Game.Enemy)
            {
                if (rectangleInfo.Intersects(enemy.RectangleInfo))
                {
                    healthPoints--;
                    rectangle.TextureRect = new IntRect(64, 0, 32, 64);
                    break;
                }
                else
                {
                    rectangle.TextureRect = new IntRect(0, 0, 32, 64);
                }
            }
            if (Game.Enemy.Count == 0)
            {
                rectangle.TextureRect = new IntRect(0, 0, 32, 64);
            }
            if (dir == DirectionSide.Left)
            {
                Vector2i textureRectPosition = new Vector2i(rectangle.TextureRect.Left + rectangle.TextureRect.Width, rectangle.TextureRect.Top);
                Vector2i textureRectSize = new Vector2i(-rectangle.TextureRect.Width, rectangle.TextureRect.Height);

                rectangle.TextureRect = new IntRect(textureRectPosition, textureRectSize);
            }
        }
        public void CheckHealth()
        {
            if (healthPoints <= 0)
            {
                IsAlive = false;
            }
        }
    }
}
