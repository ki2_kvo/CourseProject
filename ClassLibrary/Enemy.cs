﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Enemy : Creature
    {
        public Enemy(Vector2f start) : base(start)
        {
            rectangle.Texture = new Texture(Content.texture["Enemy"]);
            rectangle.TextureRect = new IntRect(0, 0, 32, 64);
            MoveSpeed = 3.5f;
            MoveSpeedAcceleration = 1f;
            JumpHeight = -10f;
            healthPoints = 100;
        }
        public override void UpdateMovement()
        {
            Vector2f interval = NewMath.Abs(rectangle.Position - Game.Player.Rectangle.Position);
            bool isEnemyInCharacter = (interval.X < Game.Player.Rectangle.Size.X / 3 && interval.Y < Game.Player.Rectangle.Size.Y / 3) ? true : false;
            bool isCharacterDetected = RectIsDetected();

            if (isEnemyInCharacter)
            {
                return;
            }

            if (RectIsDetected())
            {
                if (IsNeedToJump())
                {
                    movementSpeed.Y = JumpHeight;
                    OnGround = false;
                }

                if (dir == DirectionSide.Left)
                {
                    movementSpeed.X -= MoveSpeed;
                }
                else
                {
                    movementSpeed.X += MoveSpeed;
                }

                if (isDamaged)
                {
                    isDamaged = false;
                }

                if (movementSpeed.X > MoveSpeed)
                {
                    movementSpeed.X = MoveSpeed;
                }
                else if (movementSpeed.X < -MoveSpeed)
                {
                    movementSpeed.X = -MoveSpeed;
                }
            }
            else
            {
                if (rectangleInfo.Intersects(Game.Player.RectangleInfo))
                {
                    TurnDirectionSide();
                }
                else if (isDamaged)
                {
                    TurnDirectionSide();
                    isDamaged = false;
                }

                movementSpeed = new Vector2f(0, movementSpeed.Y);
            }
            
            if (Mouse.IsButtonPressed(Mouse.Button.Right))
            {
                movementSpeed.Y = -10.0f;
                OnGround = false;
            }
        }
        public override void MakeMove()
        {
            rectangleInfo.Left += movementSpeed.X;

            world.Map.UpdateChunksEnemyHereFlag(rectangleInfo);
            Collision(DirectionType.AlongX);

            if (onGround == false)
            {
                movementSpeed.Y += MoveSpeedAcceleration;
            }
            rectangleInfo.Top += movementSpeed.Y;
            onGround = false;

            world.Map.UpdateChunksEnemyHereFlag(rectangleInfo);
            Collision(DirectionType.AlongY);

            movementSpeed.X = 0;
        }
        public override void Collision(DirectionType type)
        {
            foreach (var item in world.Map.Chunks)
            {
                if (item.enemyHere)
                {
                    Tile tile = item.GetTileByPosition(rectangleInfo);

                    if (tile != null)
                    {
                        if (movementSpeed.X > 0 && type == DirectionType.AlongX)
                        {
                            rectangleInfo.Left = tile.Position.X - rectangleInfo.Width;
                        }
                        else if (movementSpeed.X < 0 && type == DirectionType.AlongX)
                        {
                            rectangleInfo.Left = tile.Position.X + tile.Size.X;
                        }
                        if (movementSpeed.Y > 0 && type == DirectionType.AlongY)
                        {
                            rectangleInfo.Top = tile.Position.Y - rectangleInfo.Height;
                            movementSpeed.Y = 0;
                            onGround = true;
                        }
                        else if (movementSpeed.Y < 0 && type == DirectionType.AlongY)
                        {
                            rectangleInfo.Top = tile.Position.Y + tile.Size.Y;
                            movementSpeed.Y = 0;
                        }
                    }
                }
            }
        }

        public bool RectIsDetected()
        {
            if (IsPointDetected(Game.Player.Rectangle.Position + Game.Player.Rectangle.Size / 2))
            {
                return true;
            }
            for (uint i = 0; i < Game.Player.Rectangle.GetPointCount(); i++)
            {
                if (IsPointDetected(Game.Player.Rectangle.Position + Game.Player.Rectangle.GetPoint(i)))
                {
                    return true;
                }
            }
            return false;
        }
        public bool IsPointDetected(Vector2f point)
        {
            Vector2f characterPosition = point;
            Vector2f enemyPosition;

            enemyPosition = rectangle.Position + new Vector2f(rectangle.Size.X / 2, rectangle.Size.Y / 4);

            Vector2f characterCoord = characterPosition - enemyPosition;
            Vector2f normalEnemy = NewMath.Normalize(new Vector2f((int)dir, 0));
            Vector2f normalEnemyToCharacter = NewMath.Normalize(characterCoord);
            int angle = (int)NewMath.RadiansToDegrees(Math.Acos(NewMath.Multiply(normalEnemy, normalEnemyToCharacter)));
            if (angle < 56)
            {
                Vector2f position = new Vector2f(Math.Min(Game.Player.Position.X, Position.X), Math.Min(Game.Player.Position.Y, Position.Y));
                Vector2f size = NewMath.Abs(new Vector2f(characterCoord.X, characterCoord.Y));
                FloatRect enemyToCharacterRect = new FloatRect(position, size);

                foreach (var chunk in Game.World.Map.Chunks)
                {
                    if (enemyToCharacterRect.Intersects(chunk.RectInfo))
                    {
                        foreach (var tile in chunk.Tiles)
                        {
                            if (tile.Type == TileType.InteractionGround)
                            {
                                if (NewMath.Intersects(characterPosition, Position, tile.RectInfo))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsNeedToJump()
        {
            if (onGround)
            {
                if (dir == DirectionSide.Right)
                {
                    Chunk chunk = world.Map.GetChunkByPosition(rectangle.Position + rectangle.Size + new Vector2f(3, -1));
                    Tile tile = chunk.GetTileByPosition(rectangle.Position + rectangle.Size + new Vector2f(3, -1));

                    return tile == null ? false : true;
                }
                else
                {
                    Chunk chunk = world.Map.GetChunkByPosition(rectangle.Position + new Vector2f(-3, rectangle.Size.Y - 1));
                    Tile tile = chunk.GetTileByPosition(rectangle.Position + new Vector2f(-3, rectangle.Size.Y - 1));

                    return tile == null ? false : true;
                }
            }
            return false;
        }

        public void TurnDirectionSide()
        {
            dir = dir == DirectionSide.Left ? DirectionSide.Right : DirectionSide.Left;
        }

        public override void UpdateTexture()
        {
            rectangle.TextureRect = new IntRect(0, 0, 32, 64);

            Vector2i textureRectPosition;
            Vector2i textureRectSize;
            if (dir == DirectionSide.Left)
            {
                textureRectPosition = new Vector2i(rectangle.TextureRect.Left + rectangle.TextureRect.Width, rectangle.TextureRect.Top);
                textureRectSize = new Vector2i(-rectangle.TextureRect.Width, rectangle.TextureRect.Height);

                rectangle.TextureRect = new IntRect(textureRectPosition, textureRectSize);
            }
        }
    }
}