﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class GameWindow
    {
        public static RenderWindow window;
        public static View view;

        public static FloatRect viewRect;
        public static Vector2f offset;

        public static void Load()
        {
            window = new RenderWindow(new VideoMode(GameSettings.windowSize.X, GameSettings.windowSize.Y), "2DGame");
            window.SetIcon(64, 64, Content.icon.Pixels);
            window.SetVerticalSyncEnabled(true);
            window.SetKeyRepeatEnabled(false);

            viewRect = new FloatRect(0, 0, window.Size.X, window.Size.Y);
            view = new View(viewRect);
            offset = new Vector2f(0, 0);
        }

        public static void UpdateOffset(Creature player)
        {
            if (player.RectangleInfo.Left > window.Size.X / 2)
            {
                if (player.RectangleInfo.Left + window.Size.X / 2 < Map.mostRightTile)
                {
                    offset.X = player.RectangleInfo.Left - window.Size.X / 2;
                }
                else
                {
                    offset.X = Map.mostRightTile - view.Size.X;
                }
            }
            else
            {
                offset.X = 0;
            }
            if (player.RectangleInfo.Top > window.Size.Y / 2)
            {
                if (player.RectangleInfo.Top + window.Size.Y / 2 < Map.mostBottomTile)
                {
                    offset.Y = player.RectangleInfo.Top - window.Size.Y / 2;
                }
                else
                {
                    offset.Y = Map.mostBottomTile - view.Size.Y;
                }
            }
            else
            {
                offset.Y = 0;
            }
        }

        public static void UpdateView(Creature player)
        {
            UpdateOffset(player);

            UpdateViewRectPosition();

            UpdateViewRectSize();

            view = new View(viewRect);

            window.SetView(view);
        }
        public static void UpdateViewRectPosition()
        {
            if (Map.mostRightTile < window.Size.X)
            {
                viewRect.Left = Map.mostRightTile / 2 - window.Size.X / 2;
            }
            else
            {
                viewRect.Left = offset.X;
            }
            if (Map.mostBottomTile < window.Size.Y)
            {
                viewRect.Top = Map.mostBottomTile / 2 - window.Size.Y / 2;
            }
            else
            {
                viewRect.Top = offset.Y;
            }
        }
        public static void UpdateViewRectSize()
        {
            viewRect.Width = window.Size.X;
            viewRect.Height = window.Size.Y;
        }
    }
}
