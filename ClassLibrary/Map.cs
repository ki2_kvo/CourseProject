﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Map
    {
        protected Chunk[,] chunk;
        const string PathToMap = @"..\maps\file1.map";

        public static float mostRightTile = 0;
        public static float mostBottomTile = 0;

        public Map()
        {
            LoadTilesFromFile();
        }
        public Chunk[,] Chunks
        {
            get { return chunk; }
        }

        // Сделать исключения
        private void LoadTilesFromFile()
        {
            FileStream fs = new FileStream(PathToMap, FileMode.Open, FileAccess.Read, FileShare.Read);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Binder = new CustomBinder();
            Tile[,] loadedTiles = (Tile[,])bf.Deserialize(fs);
            fs.Close();

            FindMapBorders(loadedTiles);

            CreateChunks();

            FillChunks(loadedTiles);
        }
        private void FindMapBorders(Tile[,] loadedTiles)
        {
            foreach (var tile in loadedTiles)
            {
                if (tile.positionX + tile.sizeX > mostRightTile)
                {
                    mostRightTile = tile.positionX + tile.sizeX;
                }
                if (tile.positionY + tile.sizeY > mostBottomTile)
                {
                    mostBottomTile = tile.positionY + tile.sizeY;
                }
            }
        }
        private void CreateChunks()
        {
            chunk = new Chunk[(int)(mostRightTile / Chunk.SIZE) + 1, (int)(mostBottomTile / Chunk.SIZE) + 1];

            for (int i = 0; i < chunk.GetLength(0); i++)
            {
                for (int j = 0; j < chunk.GetLength(1); j++)
                {
                    chunk[i, j] = new Chunk(i, j);
                }
            }

        }
        private void FillChunks(Tile[,] loadedTiles)
        {
            foreach (var tile in loadedTiles)
            {
                foreach (var elemChunk in chunk)
                {
                    if (elemChunk.CheckTile(tile))
                    {
                        elemChunk.SetTile(tile);
                        break;
                    }
                }
            }
        }

        public Chunk GetChunkByPosition(Vector2f point)
        {
            foreach (var item in chunk)
            {
                if (item.RectInfo.Contains(point.X, point.Y))
                {
                    return item;
                }
            }
            return null;
        }
        public Chunk GetChunkWithPlayer()
        {
            foreach (var item in chunk)
            {
                if (item.playerHere)
                {
                    return item;
                }
            }
            return null;
        }
        public void Update(Creature player, List<Creature> enemy)
        {
            foreach (var item in chunk)
            {
                item.Update(player, enemy);
            }
        }

        public void UpdateChunksPlyerHereFlag(FloatRect playerRectInfo)
        {
            foreach (var item in chunk)
            {
                item.UpdatePlayerHereFlag(playerRectInfo);
            }
        }
        public void UpdateChunksEnemyHereFlag(FloatRect enemyRectInfo)
        {
            foreach (var item in chunk)
            {
                item.UpdateEnemyHereFlag(enemyRectInfo);
            }
        }

        public void Draw()
        {
            foreach (var elemChunk in chunk)
            {
                if (elemChunk.isActive)
                {
                    elemChunk.Draw();
                }
            }
        }
    }
}
