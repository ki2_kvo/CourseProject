﻿using SFML.Graphics;
using SFML.Audio;
using System;
using System.Collections.Generic;

namespace ClassLibrary
{
    public class Content
    {
        const string CONTENT_DIR = @"..\Content\Textures\";

        public static Image icon;

        public static Dictionary<string, Texture> texture;

        public static void Load()
        {
            icon = new Image(CONTENT_DIR + "Icon.png");

            texture = new Dictionary<string, Texture>();
            texture.Add("Hero", new Texture(CONTENT_DIR + "hero2.png"));
            texture.Add("Enemy", new Texture(CONTENT_DIR + "enemy.png"));
            texture.Add("Tile1", new Texture(CONTENT_DIR + "Tile01.png"));
            texture.Add("Background1", new Texture(CONTENT_DIR + "Background02.png"));
            texture.Add("BackgroundForest", new Texture(CONTENT_DIR + "BackgroundForest.png"));
            texture.Add("MainMenuBackground", new Texture(CONTENT_DIR + "MainMenuBackground.jpg"));
        }

        public static void LoadMainMenuContent()
        {
            // Контент для главного меню
        }
    }
}