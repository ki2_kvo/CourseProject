﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;

namespace ClassLibrary
{
    public class Creature
    {
        public enum DirectionType
        {
            AlongX,
            AlongY
        }
        public enum DirectionSide
        {
            Left = -1,
            Right = 1
        }

        protected Vector2f startPosition;

        protected float MoveSpeed;
        protected float MoveSpeedAcceleration;
        protected float JumpHeight;

        protected Vector2f movementSpeed;

        protected DirectionSide dir;
        protected FloatRect rectangleInfo;
        protected RectangleShape rectangle;
        protected bool onGround;
        protected bool isDamaged;
        protected World world;

        protected int healthPoints;

        public DirectionSide Dir
        {
            get { return dir; }
        }
        public FloatRect RectangleInfo
        {
            get { return rectangleInfo; }
        }
        public Vector2f Position
        {
            get { return new Vector2f(rectangleInfo.Left, rectangleInfo.Top); }
        }
        public RectangleShape Rectangle
        {
            get { return rectangle; }
        }
        public bool OnGround
        {
            set { onGround = value; }
            get { return onGround; }
        }
        public bool IsDamaged
        {
            set { isDamaged = value; }
            get { return isDamaged; }
        }
        public int HealthPoints
        {
            set { healthPoints = value; }
            get { return healthPoints; }
        }

        public Creature() : this(new Vector2f(4 * 32, 3 * 32))
        {
        }

        public Creature(Vector2f start)
        {
            startPosition = start;
            dir = DirectionSide.Right;
            isDamaged = false;

            // rectangleInfo хранит информацию о позиции и размерах прямоугольника rectangle
            rectangleInfo = new FloatRect(startPosition, new Vector2f(24, 48));

            rectangle = new RectangleShape(new Vector2f(rectangleInfo.Width, rectangleInfo.Height));
            rectangle.Position = new Vector2f(rectangleInfo.Left, rectangleInfo.Top);

            movementSpeed = new Vector2f(0, 0);

            world = Game.World;
        }

        public Creature(Creature copyCreature)
        {
            startPosition = copyCreature.startPosition;
            MoveSpeed = copyCreature.MoveSpeed;
            MoveSpeedAcceleration = copyCreature.MoveSpeedAcceleration;
            JumpHeight = copyCreature.JumpHeight;
            movementSpeed = copyCreature.movementSpeed;
            dir = copyCreature.dir;
            rectangleInfo = copyCreature.rectangleInfo;
            rectangle = copyCreature.rectangle;
            onGround = copyCreature.onGround;
            world = copyCreature.world;
            healthPoints = copyCreature.healthPoints;
        }

        public virtual void Update()
        {
            UpdateMovement();

            MakeMove();

            UpdateTexture();

            rectangle.Position = new Vector2f(rectangleInfo.Left, rectangleInfo.Top);
        }
        public virtual void UpdateMovement()
        {
            bool isMoveLeft = Keyboard.IsKeyPressed(Keyboard.Key.A);
            bool isMoveRight = Keyboard.IsKeyPressed(Keyboard.Key.D);
            bool isMoveUp = Keyboard.IsKeyPressed(Keyboard.Key.Space);

            if (isMoveUp)
            {
                if (OnGround == true)
                {
                    movementSpeed.Y = JumpHeight;
                    OnGround = false;
                }
            }
            if (isMoveLeft || isMoveRight)
            {
                if (isMoveLeft)
                {
                    movementSpeed.X -= MoveSpeed;
                    dir = DirectionSide.Left;
                }
                else if (isMoveRight)
                {
                    movementSpeed.X += MoveSpeed;
                    dir = DirectionSide.Right;
                }

                if (movementSpeed.X > MoveSpeed)
                {
                    movementSpeed.X = MoveSpeed;
                }
                else if (movementSpeed.X < -MoveSpeed)
                {
                    movementSpeed.X = -MoveSpeed;
                }
            }
            else
            {
                movementSpeed = new Vector2f(0, movementSpeed.Y);
            }
            if (Mouse.IsButtonPressed(Mouse.Button.Right))
            {
                movementSpeed.Y = -10.0f;
                OnGround = false;
            }
        }
        public virtual void MakeMove()
        {
            rectangleInfo.Left += movementSpeed.X;

            world.Map.UpdateChunksPlyerHereFlag(rectangleInfo);
            Collision(DirectionType.AlongX);

            if (onGround == false)
            {
                movementSpeed.Y += MoveSpeedAcceleration;
            }
            rectangleInfo.Top += movementSpeed.Y;
            onGround = false;

            world.Map.UpdateChunksPlyerHereFlag(rectangleInfo);
            Collision(DirectionType.AlongY);

            movementSpeed.X = 0;
        }
        public virtual void Collision(DirectionType type)
        {
            foreach (var item in world.Map.Chunks)
            {
                if (item.playerHere)
                {
                    Tile tile = item.GetTileByPosition(rectangleInfo);

                    if (tile != null)
                    {
                        if (movementSpeed.X > 0 && type == DirectionType.AlongX)
                        {
                            rectangleInfo.Left = tile.Position.X - rectangleInfo.Width;
                        }
                        else if (movementSpeed.X < 0 && type == DirectionType.AlongX)
                        {
                            rectangleInfo.Left = tile.Position.X + tile.Size.X;
                        }
                        if (movementSpeed.Y > 0 && type == DirectionType.AlongY)
                        {
                            rectangleInfo.Top = tile.Position.Y - rectangleInfo.Height;
                            movementSpeed.Y = 0;
                            onGround = true;
                        }
                        else if (movementSpeed.Y < 0 && type == DirectionType.AlongY)
                        {
                            rectangleInfo.Top = tile.Position.Y + tile.Size.Y;
                            movementSpeed.Y = 0;
                        }
                    }
                }
            }
        }
        public virtual void UpdateTexture()
        {
        }

        public void Draw()
        {
            GameWindow.window.Draw(rectangle);
        }
    }
}