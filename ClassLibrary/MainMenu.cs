﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class MainMenu
    {
        public bool IsActive { set; get; }
        protected List<Button> button = new List<Button>(5);

        protected RectangleShape background;
        
        protected Font font = new Font("..\\Content\\Fonts\\COMIC.TTF");
        protected Text text;
        protected FloatRect textDimensions;

        public MainMenu()
        {
            OpenMainMenu();

            background = new RectangleShape((Vector2f)GameWindow.window.Size);
            background.Texture = Content.texture["MainMenuBackground"];

            for (int i = 0; i < button.Capacity; i++)
            {
                button.Add(new Button(i));
            }

            text = new Text("В разработке!", font, 32);
            text.Color = new Color(0, 0, 0, 0);
            textDimensions = text.GetGlobalBounds();
        }

        public void OpenMainMenu()
        {
            GameWindow.window.KeyPressed += Window_KeyPressed;
            GameWindow.window.MouseButtonPressed += Window_MouseButtonPressed;

            IsActive = true;
        }

        public void Update()
        {
            HighlightSelectedButton();

            // Обработчики событий сделают все остальное
        }
        public void HighlightSelectedButton()
        {
            Button.ButtonType type = Button.ButtonType.None;
            for (int i = 0; i < button.Count; i++)
            {
                type = button[i].CheckMousePosition();
                if (type == Button.ButtonType.None)
                {
                    button[i].SetDefaultMode();
                }
                else
                {
                    Button.selectedIndex = (int)type;
                }
            }
            button[Button.selectedIndex].SetSelectedMode();
        }

        private void Window_KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Return)
            {
                ActivateMenuItem();
            }
            if (e.Code == Keyboard.Key.Up && Button.selectedIndex > 0)
            {
                Button.selectedIndex--;
            }
            else if (e.Code == Keyboard.Key.Down && Button.selectedIndex < 4)
            {
                Button.selectedIndex++;
            }
        }
        private void Window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            bool isMouseOnButton = button[Button.selectedIndex].CheckMousePosition() != Button.ButtonType.None;
            if (e.Button == Mouse.Button.Left && isMouseOnButton)
            {
                ActivateMenuItem();
            }
        }
        private void DisableKeyEvents()
        {
            GameWindow.window.KeyPressed -= Window_KeyPressed;
            GameWindow.window.MouseButtonPressed -= Window_MouseButtonPressed;
        }

        public void ActivateMenuItem()
        {
            Button.ButtonType selection = (Button.ButtonType)Button.selectedIndex;
            switch (selection)
            {
                case Button.ButtonType.None:
                    break;
                case Button.ButtonType.Singleplayer:
                    OpenSingleplayer();
                    break;
                case Button.ButtonType.Multiplayer:
                    //OpenMultiplayer();
                    DisplayMessage(Button.selectedIndex);
                    break;
                case Button.ButtonType.MapEditor:
                    //OpenMapEditor();
                    DisplayMessage(Button.selectedIndex);
                    break;
                case Button.ButtonType.Options:
                    //OpenOptions();
                    DisplayMessage(Button.selectedIndex);
                    break;
                case Button.ButtonType.Exit:
                    ExitGame();
                    break;
            }
        }

        public void DebugElements()
        {
            background.Size = (Vector2f)GameWindow.window.Size;
            for (int i = 0; i < button.Count; i++)
            {
                button[i].UpdatePosition();
            }
        }

        public void OpenSingleplayer()
        {
            DisableKeyEvents();
            IsActive = false;
            Game.Load();
        }
        public void OpenMultiplayer()
        {
            // Открыть мультиплеер
        }
        public void OpenMapEditor()
        {
            // Открыть редактор карт
        }
        public void OpenOptions()
        {
            // Открыть опции
        }
        public void ExitGame()
        {
            DisableKeyEvents();
            GameWindow.window.Close();
        }

        public void DisplayMessage(int buttonIndex)
        {
            Vector2f size = new Vector2f(-textDimensions.Width - 16, button[buttonIndex].RectSize.Y / 2 - textDimensions.Height / 2);
            text.Position = button[buttonIndex].RectPositon + size;
            text.Color = new Color(0, 0, 0, 255);
        }


        public void Draw()
        {
            GameWindow.window.Draw(background);
            for (int i = 0; i < button.Count; i++)
            {
                button[i].Draw();
            }

            GameWindow.window.Draw(text);
        }
    }
}