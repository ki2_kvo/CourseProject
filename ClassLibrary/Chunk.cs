﻿using SFML.System;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Chunk
    {
        // Размер чанка по осям x и y
        public const int SIZE = 256;
        // Координаты верхней левой точки чанка
        protected Vector2f position;
        protected FloatRect rectInfo;

        // Массив тайлов
        protected List<Tile> tiles;

        public bool isActive;
        public bool playerHere;
        public bool enemyHere;

        public Chunk(int chunkIndexX, int chunkIndexY)
        {
            position = new Vector2f(chunkIndexX * SIZE, chunkIndexY * SIZE);
            rectInfo = new FloatRect(position, new Vector2f(SIZE, SIZE));

            tiles = new List<Tile>(64);

            isActive = false;
            playerHere = false;
            enemyHere = false;
        }
        public List<Tile> Tiles
        {
            get { return tiles; }
        }
        public FloatRect RectInfo
        {
            get { return rectInfo; }
        }
        public Tile GetTileByPosition(FloatRect heroRect)
        {
            foreach (var item in tiles)
            {
                FloatRect tileRect = new FloatRect(item.Position, item.Size);

                if (heroRect.Intersects(tileRect) && item.Type == TileType.InteractionGround)
                {
                    return item;
                }
            }
            return null;
        }
        public Tile GetTileByPosition(Vector2f point)
        {
            foreach (var item in tiles)
            {
                FloatRect tileRect = new FloatRect(item.Position, item.Size);

                if (tileRect.Contains(point.X, point.Y) && item.Type == TileType.InteractionGround)
                {
                    return item;
                }
            }
            return null;
        }

        public void SetTile(Tile tile)
        {
            tiles.Add(tile);
            tiles[tiles.Count - 1] = new Tile(tile);
        }

        public bool CheckTile(Tile tile)
        {
            if (rectInfo.Contains(tile.positionX, tile.positionY))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Update(Creature player, List<Creature> enemy)
        {
            if (rectInfo.Intersects(GameWindow.viewRect))
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }

            UpdatePlayerHereFlag(player.RectangleInfo);
            foreach (var item in enemy)
            {
                UpdateEnemyHereFlag(item.RectangleInfo);
            }
        }
        public void UpdatePlayerHereFlag(FloatRect playerRectInfo)
        {
            if (RectInfo.Intersects(playerRectInfo))
            {
                playerHere = true;
            }
            else
            {
                playerHere = false;
            }
        }
        public void UpdateEnemyHereFlag(FloatRect enemyRectInfo)
        {
            if (RectInfo.Intersects(enemyRectInfo))
            {
                enemyHere = true;
            }
            else
            {
                enemyHere = false;
            }
        }

        public void Draw()
        {
            foreach (var tile in tiles)
            {
                tile.Draw();
            }
        }
    }
}
