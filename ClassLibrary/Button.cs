﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Button
    {
        public enum ButtonType
        {
            None = -1,
            Singleplayer,
            Multiplayer,
            MapEditor,
            Options,
            Exit
        }

        protected static List<string> names = new List<string> { "Одиночная игра", "Мультиплеер", "Редактор карт", "Настройки", "Выход" };
        protected static Font font = new Font("..\\Content\\Fonts\\COMIC.TTF");
        protected Text text;
        protected FloatRect textDimensions;

        protected static Vector2f size = new Vector2f(256, 64);
        protected RectangleShape rect;

        public static int selectedIndex = 0;
        protected ButtonType type;

        public Vector2f RectPositon
        {
            get { return rect.Position; }
        }
        public Vector2f RectSize
        {
            get { return size; }
        }

        public Button(int number)
        {
            type = (ButtonType)number;

            text = new Text(names[number], font, 24);
            text.Color = new Color(0, 0, 0);
            text.Style = Text.Styles.Bold;
            textDimensions = text.GetGlobalBounds();

            rect = new RectangleShape(size);
            SetDefaultMode();

            UpdatePosition();
        }

        public void UpdatePosition()
        {
            int positionX = (int)(GameWindow.window.Size.X / 2 - textDimensions.Width / 2);

            int distanceBetweenButtonsY = (int)((GameWindow.window.Size.Y - names.Count * size.Y) / (names.Count + 1));
            int positionY = (int)(distanceBetweenButtonsY * ((int)type + 1) + size.Y * (int)type);

            text.Position = new Vector2f(positionX, positionY);

            float posX = text.Position.X + textDimensions.Width / 2 - size.X / 2 + 1;
            float posY = text.Position.Y + textDimensions.Height / 2 - size.Y / 2 + 6;
            rect.Position = new Vector2f(posX, posY);
        }

        public ButtonType CheckMousePosition()
        {
            Vector2i mousePosition = Mouse.GetPosition(GameWindow.window);

            Vector2f position = rect.Position - new Vector2f(8, 8);
            Vector2f size = rect.Size + new Vector2f(16, 16);
            FloatRect rectDimensions = new FloatRect(position, size);

            bool isMouseOnButton = rectDimensions.Contains(mousePosition.X, mousePosition.Y);
            if (isMouseOnButton)
            {
                return type;
            }
            return ButtonType.None;
        }

        public void SetSelectedMode()
        {
            rect.OutlineThickness = 8f;
            rect.OutlineColor = new Color(93, 75, 216, 255);
            rect.FillColor = new Color(255, 214, 64, 255);
        }
        public void SetDefaultMode()
        {
            rect.OutlineThickness = 4f;
            rect.OutlineColor = new Color(93, 75, 216, 192);
            rect.FillColor = new Color(255, 214, 64, 192);
        }

        public void Draw()
        {
            GameWindow.window.Draw(rect);
            GameWindow.window.Draw(text);
        }
    }
}