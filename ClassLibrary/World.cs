﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class World
    {
        protected Map map;

        protected RectangleShape background;

        // Потом добавить противников

        public World()
        {
            map = new Map();
            background = new RectangleShape(GameWindow.view.Size);
            background.Texture = Content.texture["BackgroundForest"];
        }
        
        public Map Map
        {
            get { return map; }
        }

        public void DebugWorld()
        {
            //background.Size = (Vector2f)GameWindow.window.Size;
        }

        public void Update(Creature player, List<Creature> enemy)
        {
            GameWindow.UpdateView(player);
            map.Update(player, enemy);
        }

        public void Draw()
        {
            //GameWindow.window.Draw(background);
            map.Draw();
        }
    }
}
