﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace ClassLibrary
{
    public class GameSettings
    {
        //Скорость игры
        public static int gameSpeed;
        //Размеры экрана
        public static Vector2u windowSize;

        public int GameSpeed
        {
            set { gameSpeed = value; }
            get { return gameSpeed; }
        }

        // Потом поменять
        public static Vector2u WindowSizeToTileSize
        {
            get { return windowSize / 32; }
        }
        

        public static void Load()
        {
            gameSpeed = 30;
            windowSize.X = 800;
            windowSize.Y = 600;
        }

        public static void UpdateWindowSize()
        {
            windowSize = GameWindow.window.Size;
        }

        // Сделать чтение настроек из файла
    }
}
