﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Bullet
    {
        protected Creature shooter;
        protected CircleShape circle;
        protected Vector2f direction;
        protected float speed;
        protected bool isDestroyed;

        public Bullet(Vector2f dir)
        {
            isDestroyed = false;
            shooter = Game.Player;
            circle = new CircleShape(2f);
            circle.Origin = new Vector2f(circle.Radius, circle.Radius);
            circle.FillColor = new Color(255, 255, 255);
            direction = dir;
            Vector2f position;
            if (shooter.Dir == Creature.DirectionSide.Right)
            {
                position = new Vector2f(shooter.Position.X + shooter.Rectangle.Size.X, shooter.Position.Y + shooter.Rectangle.Size.Y / 3);
            }
            else
            {
                position = new Vector2f(shooter.Position.X, shooter.Position.Y + shooter.Rectangle.Size.Y / 3);
            }
            circle.Position = position;
            
            speed = 20f;
        }

        public bool IsDestroyed
        {
            get { return isDestroyed; }
        }
        public CircleShape Circle
        {
            get { return circle; }
        }

        public void Update(float interpolation)
        {
            if (circle.Position.X < 0 || circle.Position.Y < 0 || circle.Position.X > Map.mostRightTile || circle.Position.Y > Map.mostBottomTile)
            {
                isDestroyed = true;
            }
            circle.Position = circle.Position + new Vector2f(direction.X * speed, direction.Y * speed);

            Collision();
        }

        public void Collision()
        {
            Chunk chunk = Game.World.Map.GetChunkByPosition(circle.Position);
            foreach (var item in chunk.Tiles)
            {
                Tile tile = chunk.GetTileByPosition(circle.Position);

                if (tile != null)
                {
                    isDestroyed = true;
                    return;
                }
            }
            foreach (var item in Game.Enemy)
            {
                if (item.RectangleInfo.Contains(circle.Position.X, circle.Position.Y))
                {
                    item.HealthPoints -= 30;
                    item.IsDamaged = true;
                    item.Rectangle.TextureRect = new IntRect(64, 0, 32, 64);
                    isDestroyed = true;
                }
            }
        }

        public void Draw()
        {
            GameWindow.window.Draw(circle);
        }
    }
}
