﻿using SFML.Graphics;
using SFML.System;
using System;

namespace ClassLibrary
{
    public enum TileType
    {
        None,
        Background,
        InteractionGround
    }
    [Serializable]
    public class Tile
    {
        public float positionX;
        public float positionY;
        public float sizeX;
        public float sizeY;

        protected TileType type;
        [NonSerialized]
        protected RectangleShape rectangle;
        [NonSerialized]
        protected Vector2f position;
        [NonSerialized]
        protected Vector2f size;
        protected string textureName;

        public Tile(TileType type)
        {
            this.type = type;

            size = new Vector2f(32, 32);

            rectangle = new RectangleShape(size);

            UpdateTexture();
        }
        public Tile(Tile copyTile)
        {
            type = copyTile.type;
            position = new Vector2f(copyTile.positionX, copyTile.positionY);
            size = new Vector2f(copyTile.sizeX, copyTile.sizeY);
            rectangle = new RectangleShape(size);
            rectangle.Position = position;
            UpdateTexture();
        }
        public void UpdateTexture()
        {
            switch (type)
            {
                case TileType.InteractionGround:
                    {
                        rectangle.Texture = Content.texture["Tile1"];
                        textureName = "Tile1";
                        break;
                    }
                case TileType.Background:
                    {
                        rectangle.Texture = Content.texture["Background1"];
                        textureName = "Background1";
                        break;
                    }
                case TileType.None:
                    {
                        rectangle.Texture = null;
                        textureName = null;
                        break;
                    }
            }
        }

        public TileType Type
        {
            get { return type; }
        }
        public RectangleShape Rectangle
        {
            get { return rectangle; }
        }
        public FloatRect RectInfo
        {
            get { return new FloatRect(position, size); }
        }
        public Vector2f Size
        {
            get { return size; }
        }
        public Vector2f Position
        {
            get { return position; }
        }

        public void Draw()
        {
            GameWindow.window.Draw(rectangle);
        }
    }
}