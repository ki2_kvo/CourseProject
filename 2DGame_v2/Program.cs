using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Threading;
using ClassLibrary;

namespace _2DGame_v2
{
    public static class Program
    {
        public static RenderWindow window;
        public static MainMenu mainMenu;

        static void Main(string[] args)
        {
            GameSettings.Load();
            Content.Load();
            GameWindow.Load();
            GameTimer.Start();


            window = GameWindow.window;

            window.Closed += window_Closed;
            window.Resized += Window_Resized;
            window.KeyPressed += Window_KeyPressed;

            mainMenu = new MainMenu();

            #region fps, time
            /////////////////////////////////////////////////////

            const int TICKS_PER_SECOND = 30;
            const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
            const int MAX_FRAMESKIP = 5;
            int loops;
            float interpolation = 0;

            /////////////////////////////////////////////////////
            #endregion

            while (window.IsOpen)
            {
                window.DispatchEvents();

                loops = 0;
                while (GameTimer.clock.ElapsedTime.AsMilliseconds() > GameTimer.time && loops < MAX_FRAMESKIP)
                {
                    if (mainMenu.IsActive)
                    {
                        mainMenu.Update();
                    }
                    else
                    {
                        Game.Update(interpolation);
                        CheckEndGame();
                    }
                    GameTimer.time += SKIP_TICKS;
                    loops++;
                }
                interpolation = (GameTimer.clock.ElapsedTime.AsMilliseconds() + SKIP_TICKS - GameTimer.time) / (float)SKIP_TICKS;

                window.Clear(Color.Black);

                if (mainMenu.IsActive)
                {
                    mainMenu.Draw();
                }
                else
                {
                    Game.Draw();
                }
                window.Display();
            }
        }

        public static void CheckEndGame()
        {
            if (Game.Enemy.Count == 0)
            {
                MessageSystem.DisplayWinEnd();
                Game.Clear();
                mainMenu = new MainMenu();
                window.SetView(new View(new FloatRect(0, 0, window.Size.X, window.Size.Y)));
                GameSettings.UpdateWindowSize();
                mainMenu.DebugElements();
            }
            else if (!Game.Player.IsAlive)
            {
                MessageSystem.DisplayLoseEnd();
                Game.Clear();
                mainMenu = new MainMenu();
                window.SetView(new View(new FloatRect(0, 0, window.Size.X, window.Size.Y)));
                GameSettings.UpdateWindowSize();
                mainMenu.DebugElements();
            }
        }

        private static void Window_KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Escape && !mainMenu.IsActive)
            {
                Game.Clear();
                mainMenu = new MainMenu();
                window.SetView(new View(new FloatRect(0, 0, window.Size.X, window.Size.Y)));
                GameSettings.UpdateWindowSize();
                mainMenu.DebugElements();
            }
        }

        private static void Window_Resized(object sender, SizeEventArgs e)
        {
            if (mainMenu.IsActive)
            {
                window.SetView(new View(new FloatRect(0, 0, e.Width, e.Height)));
                GameSettings.UpdateWindowSize();
                mainMenu.DebugElements();
            }
            else
            {
                GameWindow.UpdateViewRectSize();
            }
        }
        private static void window_Closed(object sender, EventArgs e)
        {
            window.Close();
        }
    }
}